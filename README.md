# Mimesis API

A back-end server application to store player data for [Mimesis](https://gitlab.com/blink7/mimesis) game with a front-end web view. Based on Spring Framework and Vue.js.

## License
Not for personal or comercial use!
