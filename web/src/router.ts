import { createRouter, createWebHistory } from 'vue-router';

import Home from './components/Home.vue'
import DevBlog from './components/DevBlog.vue'
import About from './components/About.vue'
import PlayerList from './components/PlayerList.vue'
import WorldStats from './components/WorldStats.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'Mimesis - Home',
            component: Home,
            meta: {
                title: '>> HomePage.gag'
            }
        },
        {
            path: '/blog',
            name: 'Mimesis - DevBlog',
            component: DevBlog,
            meta: {
                title: '>> DevBlog.gag'
            }
        },
        {
            path: '/about',
            name: 'Mimesis - About',
            component: About,
            meta: {
                title: '>> About.gag'
            }
        },
        {
            path: '/player_list',
            name: 'Player list',
            component: PlayerList,
            meta: {
                title: '>> PlayerList.gag'
            }
        },
        {
            path: '/stats',
            name: 'World stats',
            component: WorldStats,
            meta: {
                title: '>> WorldStats.gag'
            }
        }
    ]
});

export default router;
