import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import App from './App.vue'
import router from './router'
import VueGtag from 'vue-gtag'

export interface Locale {
    ja: any,
    en: any,
    ru: any
}

export const messages = {
    ja: require('@/locale/ja.json'),
    en: require('@/locale/en.json'),
    ru: require('@/locale/ru.json')
}

const i18n = createI18n({
    locale: navigator.language.split('-')[0],
    fallbackLocale: 'en',
    messages,
})

createApp(App)
    .use(router)
    .use(i18n)
    .use(VueGtag, {
        config: { id: 'UA-27831254-1' }
    }, router)
    .mount('#app')
