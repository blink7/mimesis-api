package com.prophunt.business;

import com.prophunt.FileStorageProperties;
import com.prophunt.persistence.ErrorCode;
import com.prophunt.persistence.PropHuntError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.UUID;

@Service
@Slf4j
public class LocalStorageService implements StorageService {

    private final Path fileStorageLocation;

    @Autowired
    public LocalStorageService(FileStorageProperties fileStorageProperties) {
        fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
        try {
            Files.createDirectories(fileStorageLocation);
        } catch (Exception e) {
            throw new PropHuntError(ErrorCode.CreateDirectoryError, e);
        }
    }

    @Override
    public String store(MultipartFile file) {
        final String originalFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        final String uuid = UUID.randomUUID().toString();
        final Path targetLocation = fileStorageLocation.resolve(uuid);
        try {
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new PropHuntError(ErrorCode.FileStoreError, e, originalFileName);
        }

        return uuid;
    }

    @Override
    public Path load(String filename) {
        return fileStorageLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        final Path fileLocation = load(filename);
        return new FileSystemResource(fileLocation);
    }

    @Override
    public void delete(String filename) {
        final Path fileLocation = fileStorageLocation.resolve(filename);
        try {
            Files.delete(fileLocation);
        } catch (IOException e) {
//            e.printStackTrace();
            // Ignore if does not exist
        }
    }
}
