package com.prophunt.business;

import com.prophunt.persistence.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

@Slf4j
@Transactional
@Service
public class CustomizationService {

    private final CustomizationRepository customizationRepository;

    @Autowired
    public CustomizationService(CustomizationRepository customizationRepository) {
        this.customizationRepository = customizationRepository;
    }

    public PropColorScheme getPropColorScheme(@NotNull final String uniqueNetId) {
        return customizationRepository.findPropColorSchemeByPlayerId(uniqueNetId)
                .orElseThrow(() -> new PropHuntError(ErrorCode.PlayerStatsNotFoundError, uniqueNetId));
    }

    public void updatePropColorScheme(@NotNull final String uniqueNetId, @NotNull final PropColorScheme scheme) {
        final var currentScheme = customizationRepository.findPropColorSchemeByPlayerId(uniqueNetId)
                .orElseThrow(() -> new PropHuntError(ErrorCode.PlayerStatsNotFoundError, uniqueNetId))
                .updateStats(scheme);
        customizationRepository.saveOrUpdate(currentScheme);
    }
}
