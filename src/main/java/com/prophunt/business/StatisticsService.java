package com.prophunt.business;

import com.prophunt.persistence.*;
import com.prophunt.persistence.dto.RatingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@Transactional
@Service
public class StatisticsService {

    private final StatisticsRepository statisticsRepository;
    private final PlayerRepository playerRepository;

    @Autowired
    public StatisticsService(StatisticsRepository statisticsRepository, PlayerRepository playerRepository) {
        this.statisticsRepository = statisticsRepository;
        this.playerRepository = playerRepository;
    }

    public List<RatingResponse> getTodayRating(@NotNull RatingType type, @NotNull Integer limit, @NotNull String uniqueNetId) {
        if (!playerRepository.checkPlayerExist(uniqueNetId)) {
            throw new PropHuntError(ErrorCode.PlayerNotFoundError, uniqueNetId);
        }

        return statisticsRepository.getTodayRating(type, limit, uniqueNetId);
    }

    public PlayerStats getStats(@NotNull final String uniqueNetId) {
        return statisticsRepository.findStatsByPlayerId(uniqueNetId, false)
                .orElseThrow(() -> new PropHuntError(ErrorCode.PlayerStatsNotFoundError, uniqueNetId));
    }

    public void updateStats(@NotNull final String uniqueNetId, @NotNull final PlayerStats stats) {
        statisticsRepository.saveOrUpdate(getStats(uniqueNetId).updateStats(stats));
    }

    //Fires at 12 PM every day
    @Scheduled(cron = "0 0 12 * * ?")
    public void clearTodayStatistics() {
        statisticsRepository.clearTodayStatistics();
        log.info("Today's statistics are cleared out");
    }
}
