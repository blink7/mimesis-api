package com.prophunt.business;

import com.prophunt.persistence.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Slf4j
@Transactional
@Service
public class SprayService {

    private final SprayRepository sprayRepository;
    private final StorageService storageService;
    private final PlayerService playerService;

    @Autowired
    public SprayService(SprayRepository sprayRepository, StorageService storageService, PlayerService playerService) {
        this.sprayRepository = sprayRepository;
        this.storageService = storageService;
        this.playerService = playerService;
    }

    public boolean isSprayExist(@NotNull final String uniqueNetId, final String md5) {
        return sprayRepository.isSprayExist(uniqueNetId, md5);
    }

    public String storeSpray(@NotNull final String uniqueNetId, final MultipartFile sprayFile) {
        final var player = playerService.getPlayer(uniqueNetId);

        Spray spray = sprayRepository.findByPlayerId(uniqueNetId)
                .orElseGet(() -> new Spray().setPlayer(player));

        final String prevSprayName = spray.getFileName();
        if (Strings.isNotBlank(prevSprayName)) storageService.delete(prevSprayName);

        final String newSprayName = storageService.store(sprayFile);
        spray.setFileName(newSprayName);

        try {
            final String md5Hex = DigestUtils.md5Hex(sprayFile.getInputStream());
            spray.setMd5(md5Hex);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sprayRepository.saveAndFlush(spray);
        return newSprayName;
    }

    public Resource loadSpray(@NotNull final String uniqueNetId) {
        final Spray spray = sprayRepository.findByPlayerId(uniqueNetId)
                .orElseThrow(() -> new PropHuntError(ErrorCode.NotFound))
                .setLastUseDate(Timestamp.valueOf(LocalDateTime.now()));

        sprayRepository.saveOrUpdate(spray);

        return storageService.loadAsResource(spray.getFileName());
    }

    @Scheduled(cron = "0 0 12 * * ?")
    public void clearOldSprays() {
        sprayRepository.removeOldSprays().forEach(sprayName -> {
            storageService.delete(sprayName);
            log.info("Spray [{}] has been deleted", sprayName);
        });
    }
}
