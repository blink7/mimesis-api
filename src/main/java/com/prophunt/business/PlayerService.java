package com.prophunt.business;

import com.prophunt.persistence.*;
import com.prophunt.persistence.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Transactional
@Service
public class PlayerService {

    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public PlayerResponse createPlayer(@NotNull final PlayerRequest playerRequest) {
        if (playerRepository.checkPlayerExist(playerRequest.getUniqueNetId())) {
            throw new PropHuntError(ErrorCode.PlayerAlreadyExistsError);
        }

        log.debug("Create new Player {}", playerRequest);

        var newPlayer = new Player()
                .setUniqueNetId(playerRequest.getUniqueNetId())
                .setPlayerName(playerRequest.getPlayerName())
                .setOnlineSubsystem(playerRequest.getOnlineSubsystem())
                .setupNewPlayer();

        return PlayerResponse.fromModel(playerRepository.saveOrUpdate(newPlayer));
    }

    public Player getPlayer(@NotNull final String uniqueNetId) {
        Player foundPlayer = playerRepository.findByUniqueNetId(uniqueNetId)
                .orElseThrow(() -> new PropHuntError(ErrorCode.PlayerNotFoundError, uniqueNetId))
                .setLastLogin(Timestamp.valueOf(LocalDateTime.now()));
        playerRepository.saveOrUpdate(foundPlayer);

        return foundPlayer;
    }

    public PlayerExp getExp(@NotNull final String uniqueNetId) {
        return PlayerExp.fromModel(playerRepository.findByUniqueNetId(uniqueNetId).orElse(new Player()));
    }

    public void updateExp(@NotNull final String uniqueNetId, @NotNull final PlayerExp playerExp) {
        final var currentPlayer = playerRepository.findByUniqueNetId(uniqueNetId)
                .orElseThrow(() -> new PropHuntError(ErrorCode.PlayerNotFoundError, uniqueNetId))
                .setCharacterLevel(playerExp.getLvl())
                .setCurrentXP(playerExp.getCurrentXP())
                .setNeededXP(playerExp.getNeededXP());
        playerRepository.saveOrUpdate(currentPlayer);
    }

    public void updateAbilities(@NotNull final Player player) {
        var currentPlayer = playerRepository.findByUniqueNetId(player.getUniqueNetId())
                .orElseThrow(() -> new PropHuntError(ErrorCode.PlayerNotFoundError, player.getId()));

        if (currentPlayer.updateAbilities(player)) {
            playerRepository.saveOrUpdate(currentPlayer);
        }
    }

    public List<PlayerItem> getPlayerItems() {
        return playerRepository.getPlayerItems();
    }

    public boolean removePlayer(@NotNull final Long playerId) {
        return playerRepository.removeById(playerId);
    }
}
