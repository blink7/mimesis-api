package com.prophunt.persistence;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@Accessors(chain = true)
@Getter
@Setter
@ToString
@Entity
public class Spray extends LongModel implements Serializable {

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @ToString.Exclude
    @Serial
    private static final long serialVersionUID = 1L;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long sprayId;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false, unique = true, updatable = false)
    private Player player;

    @Column(length = 48)
    private String fileName;

    @Column(length = 32)
    private String md5;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, updatable = false)
    private Date uploadDate = new Date();

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date lastUseDate = new Date();

    @Override
    public Long getId() {
        return sprayId;
    }
}
