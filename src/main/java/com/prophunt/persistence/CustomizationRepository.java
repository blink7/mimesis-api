package com.prophunt.persistence;

import com.prophunt.persistence.jpa.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public class CustomizationRepository extends AbstractRepository<PropColorScheme> {

    @Override
    protected Class<PropColorScheme> getEntityType() {
        return PropColorScheme.class;
    }

    public Optional<PropColorScheme> findPropColorSchemeByPlayerId(@NotNull String uniqueNetId) {
        final var builder = getCriteriaBuilder();
        final var query = createQuery();
        final var player = query.from(Player.class);

        return Optional.ofNullable(getSingleResult(query
                .select(player.get(Player_.PROP_COLOR_SCHEME))
                .where(builder.equal(player.get(Player_.UNIQUE_NET_ID), uniqueNetId))));
    }
}
