package com.prophunt.persistence;

import com.blazebit.persistence.ObjectBuilder;
import com.blazebit.persistence.SelectBuilder;
import com.prophunt.persistence.dto.RatingResponse;
import com.prophunt.persistence.jpa.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public class StatisticsRepository extends AbstractRepository<PlayerStats> {

    @Override
    protected Class<PlayerStats> getEntityType() {
        return PlayerStats.class;
    }

    public Optional<PlayerStats> findStatsByPlayerId(@NotNull String uniqueNetId, boolean today) {
        final var builder = getCriteriaBuilder();
        final var query = createQuery();
        final var player = query.from(Player.class);

        return Optional.ofNullable(getSingleResult(query
                .select(today ? player.get(Player_.TODAY_PLAYER_STATS) : player.get(Player_.PLAYER_STATS))
                .where(builder.equal(player.get(Player_.UNIQUE_NET_ID), uniqueNetId))));
    }

    public List<RatingResponse> getTodayRating(@NotNull RatingType type, Integer limit, @NotNull String uniqueNetId) {
        final String score = switch (type) {
            case prop -> PlayerStats_.PROP_XP;
            case hunter -> PlayerStats_.HUNTER_XP;
            case kills -> PlayerStats_.KILLS;
            case deaths -> PlayerStats_.DEATHS;
            case wins -> PlayerStats_.WINS;
            default -> PlayerStats_.LOSSES;
        };

        return getCriteriaBuilderFactory().create(getEntityManager(), Tuple.class)
                .from(Player.class)
                .distinct()
                .leftJoin(Player_.TODAY_PLAYER_STATS, "todayStats")
                .selectNew(new ObjectBuilder<RatingResponse>() {
                    @Override
                    public <X extends SelectBuilder<X>> void applySelects(X queryBuilder) {
                        queryBuilder
                                .select(Player_.UNIQUE_NET_ID)
                                .select(Player_.PLAYER_NAME)
                                .select("todayStats." + score);
                    }

                    @Override
                    public RatingResponse build(Object[] tuple) {
                        return new RatingResponse(
                                (String) tuple[1],
                                (Integer) tuple[2],
                                uniqueNetId.equals(tuple[0]));
                    }

                    @Override
                    public List<RatingResponse> buildList(List<RatingResponse> list) {
                        return list;
                    }
                })
                .whereOr()
                    .where(Player_.UNIQUE_NET_ID).eq(uniqueNetId)
                    .where(Player_.PLAYER_ID).in()
                        .from(Player.class)
                        .leftJoin(Player_.TODAY_PLAYER_STATS, "todayStats2")
                        .select(Player_.PLAYER_ID)
                            .where(Player_.UNIQUE_NET_ID).notEq(uniqueNetId)
                        .orderByDesc("todayStats2." + score)
                        .setMaxResults(limit - 1)
                    .end()
                .endOr()
                .orderByDesc("todayStats." + score)
                .getResultList();
    }

    public void clearTodayStatistics() {
        final var builder = getCriteriaBuilder();
        final var statsCriteriaUpdate = builder.createCriteriaUpdate(getEntityType());
        final var statsRoot = statsCriteriaUpdate.from(getEntityType());

        final var todayStatsSubquery = statsCriteriaUpdate.subquery(Integer.class);
        final var playerRoot = todayStatsSubquery.from(Player.class);
        todayStatsSubquery.select(playerRoot.get(Player_.TODAY_PLAYER_STATS));

        getEntityManager().createQuery(
                        statsCriteriaUpdate
                                .set(statsRoot.get(PlayerStats_.KILLS), 0)
                                .set(statsRoot.get(PlayerStats_.DEATHS), 0)
                                .set(statsRoot.get(PlayerStats_.TAUNTS), 0)
                                .set(statsRoot.get(PlayerStats_.TRANSFORMS), 0)
                                .set(statsRoot.get(PlayerStats_.SURVIVAL_TIME), 0)
                                .set(statsRoot.get(PlayerStats_.WINS), 0)
                                .set(statsRoot.get(PlayerStats_.LOSSES), 0)
                                .set(statsRoot.get(PlayerStats_.PROP_XP), 0)
                                .set(statsRoot.get(PlayerStats_.HUNTER_XP), 0)
                                .where(builder.equal(statsRoot.get(PlayerStats_.PLAYER_STATS_ID), builder.any(todayStatsSubquery))))
                .executeUpdate();

//        getEntityManager().createStoredProcedureQuery("clear_today_statistics").execute();
    }
}
