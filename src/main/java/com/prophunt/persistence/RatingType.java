package com.prophunt.persistence;

public enum RatingType {
    prop, hunter, kills, deaths, wins, loses
}
