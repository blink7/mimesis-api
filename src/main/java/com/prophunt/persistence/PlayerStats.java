package com.prophunt.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;

@Accessors(chain = true)
@Getter
@Setter
@ToString
@Entity
public class PlayerStats extends LongModel implements Serializable
{
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @ToString.Exclude
    @Serial
    private static final long serialVersionUID = 1L;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long playerStatsId;

    @Column(nullable = false)
    private Integer kills = 0;

    @Column(nullable = false)
    private Integer deaths = 0;

    @Column(nullable = false)
    private Integer taunts = 0;

    @Column(nullable = false)
    private Integer transforms = 0;

    @Column(nullable = false)
    private Integer survivalTime = 0;

    @Column(nullable = false)
    private Integer wins = 0;

    @Column(nullable = false)
    private Integer losses = 0;

    @Column(nullable = false)
    private Integer propXP = 0;

    @Column(nullable = false)
    private Integer hunterXP = 0;

    @JsonIgnore
    @Override
    public Long getId()
    {
        return playerStatsId;
    }

    public PlayerStats updateStats(final PlayerStats stats) {
        if (stats.kills != null) {
            this.kills += stats.kills;
        }
        if (stats.deaths != null) {
            this.deaths += stats.deaths;
        }
        if (stats.taunts != null) {
            this.taunts += stats.taunts;
        }
        if (stats.transforms != null) {
            this.transforms += stats.transforms;
        }
        if (stats.survivalTime != null) {
            this.survivalTime += stats.survivalTime;
        }
        if (stats.wins != null) {
            this.wins += stats.wins;
        }
        if (stats.losses != null) {
            this.losses += stats.losses;
        }
        if (stats.propXP != null) {
            this.propXP += stats.propXP;
        }
        if (stats.hunterXP != null) {
            this.hunterXP += stats.hunterXP;
        }

        return this;
    }
}
