package com.prophunt.persistence;

public class PropHuntError extends RuntimeException {

    private static final long serialVersionUID = -710488268845379700L;

    private final Object[] args;
    private final ErrorCode errorCode;
    private final String ownMessage;
    private static final String MESSAGE = "PropHunt API error";

    public PropHuntError(final ErrorCode errorCode, final Throwable cause) {
        super(cause.getMessage(), cause);
        this.errorCode = errorCode;
        this.args = null;
        this.ownMessage = null;
    }

    public PropHuntError(final ErrorCode errorCode) {
        super(MESSAGE);
        this.errorCode = errorCode;
        this.args = null;
        this.ownMessage = null;
    }

    public PropHuntError(final ErrorCode errorCode, final Object... args) {
        super(MESSAGE);
        this.errorCode = errorCode;
        this.args = args;
        this.ownMessage = null;
    }

    public PropHuntError(final ErrorCode errorCode, final Throwable cause, final Object... args) {
        super(cause.getMessage(), cause);
        this.errorCode = errorCode;
        this.args = args;
        this.ownMessage = null;
    }

    public PropHuntError(final ErrorCode errorCode, final String ownMessage) {
        super(ownMessage);
        this.errorCode = errorCode;
        this.args = null;
        this.ownMessage = ownMessage;
    }

    public PropHuntError(final ErrorCode errorCode, final Throwable cause, final String ownMessage) {
        super(cause.getMessage(), cause);
        this.errorCode = errorCode;
        this.args = null;
        this.ownMessage = ownMessage;
    }

    public Object[] getArgs() {
        return args;
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    public String getOwnMessage() {
        return this.ownMessage;
    }
}
