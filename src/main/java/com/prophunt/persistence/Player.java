package com.prophunt.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@Accessors(chain = true)
@Getter
@Setter
@ToString
@Entity
public class Player extends LongModel implements Serializable {

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @ToString.Exclude
    @Serial
    private static final long serialVersionUID = 1L;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long playerId;

    @Column(nullable = false, unique = true, updatable = false, length = 64)
    private String uniqueNetId;

    @Column(nullable = false, length = 32)
    private String playerName;

//    @Type(type = MyEnumType.ENUM_TYPE_NAME, parameters = @Parameter(name = "enum", value = "OnlineSubsystem"))
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, updatable = false)
    private OnlineSubsystem onlineSubsystem;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, updatable = false)
    private Date created = new Date();

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date lastLogin = new Date();

    @Column(nullable = false)
    private Integer characterLevel = 80;

    @Column(nullable = false)
    private Integer currentXP = 0;

    @Column(nullable = false)
    private Integer neededXP = 0;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false, orphanRemoval = true)
    @JoinColumn(nullable = false, unique = true, updatable = false)
    private PlayerStats playerStats;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false, orphanRemoval = true)
    @JoinColumn(nullable = false, unique = true, updatable = false)
    private PlayerStats todayPlayerStats;

    @Column(length = 64)
    private String hunterSkillId;

    @Column(length = 64)
    private String propSkillId;

    @Column(length = 64)
    private String customWeaponId;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false, orphanRemoval = true)
    @JoinColumn(nullable = false, unique = true, updatable = false)
    private PropColorScheme propColorScheme;

    @JsonIgnore
    @Override
    public Long getId() {
        return playerId;
    }

    public boolean updateAbilities(Player player) {
        boolean dirty = false;

        if (!StringUtils.equals(hunterSkillId, player.hunterSkillId)) {
            hunterSkillId = player.hunterSkillId;
            dirty = true;
        }
        if (!StringUtils.equals(propSkillId, player.propSkillId)) {
            propSkillId = player.propSkillId;
            dirty = true;
        }
        if (!StringUtils.equals(customWeaponId, player.customWeaponId)) {
            customWeaponId = player.customWeaponId;
            dirty = true;
        }

        return dirty;
    }

    public Player setupNewPlayer() {
        playerStats = new PlayerStats();
        todayPlayerStats = new PlayerStats();
        propColorScheme = new PropColorScheme();
        return this;
    }
}
