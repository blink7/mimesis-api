package com.prophunt.persistence.dto;

import com.prophunt.persistence.OnlineSubsystem;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
public class PlayerRequest {
    private final String uniqueNetId;
    private final String playerName;
    private final OnlineSubsystem onlineSubsystem;
}
