package com.prophunt.persistence.dto;

import com.prophunt.persistence.OnlineSubsystem;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor
public class PlayerItem {
    private final Long playerId;
    private final String playerName;
    private final Integer level;
    private final OnlineSubsystem onlineSubsystem;
}
