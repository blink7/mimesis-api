package com.prophunt.persistence.dto;

import com.prophunt.persistence.Player;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@Getter
@RequiredArgsConstructor
public class PlayerResponse {
    private final String uniqueNetId;
    private final Date created;
    private final Integer characterLevel;
    private final String hunterSkillId;
    private final String propSkillId;
    private final String customWeaponId;

    public static PlayerResponse fromModel(Player player) {
        return new PlayerResponse(
                player.getUniqueNetId(),
                player.getCreated(),
                player.getCharacterLevel(),
                player.getHunterSkillId(),
                player.getPropSkillId(),
                player.getCustomWeaponId());
    }
}
