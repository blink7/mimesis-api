package com.prophunt.persistence.dto;

import com.prophunt.persistence.Player;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PlayerExp {
    private final Integer lvl;
    private final Integer currentXP;
    private final Integer neededXP;

    public static PlayerExp fromModel(Player player) {
        return new PlayerExp(
                player.getCharacterLevel(),
                player.getCurrentXP(),
                player.getNeededXP());
    }
}
