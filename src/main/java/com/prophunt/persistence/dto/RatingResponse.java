package com.prophunt.persistence.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RatingResponse {
    private final String playerName;
    private final Integer score;
    private final Boolean owner;
}
