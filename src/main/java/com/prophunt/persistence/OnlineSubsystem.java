package com.prophunt.persistence;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum OnlineSubsystem {
    NULL, STEAM;

    @JsonCreator
    public static OnlineSubsystem fromString(String type) {
        if(type == null || type.isBlank()) {
            return NULL;
        }

        return OnlineSubsystem.valueOf(type.toUpperCase());
    }
}
