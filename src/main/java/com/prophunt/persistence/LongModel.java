package com.prophunt.persistence;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxyHelper;

import java.io.Serializable;

public abstract class LongModel implements Serializable
{
    private static final long serialVersionUID = -8212660890845368457L;

    public abstract Long getId();

    public static <T extends LongModel> boolean isNew(final T model)
    {
        return model != null && model.getId() == null;
    }

    public static <T extends LongModel> boolean accessAllowed(final T model)
    {
        return Hibernate.isInitialized(model) && model != null;
    }

    public static boolean modelEquals(final LongModel model, final Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (HibernateProxyHelper.getClassWithoutInitializingProxy(model) != HibernateProxyHelper.getClassWithoutInitializingProxy(obj))
        {
            return false;
        }
        if (model == obj)
        {
            return true;
        }
        LongModel other = (LongModel) obj;
        return model.getId() != null && new EqualsBuilder().append(model.getId(), other.getId()).isEquals();
    }

    public static int modelHashCode(final LongModel model)
    {
        return new HashCodeBuilder().append(model.getId()).toHashCode();
    }

    protected boolean checkDirty(final Object o1, final Object o2)
    {
        if (o1 == null && o2 == null)
        {
            return false;
        }
        if (o1 == null)
        {
            return true;
        }
        if (o2 == null)
        {
            return true;
        }
        return !o1.equals(o2);
    }
}
