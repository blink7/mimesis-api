package com.prophunt.persistence.jpa.hibernate;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;

import java.util.List;

public class PgGetJsonValueByKey implements SQLFunction
{
    @Override
    @SuppressWarnings("rawtypes")
    public String render(final Type type, final List args, final SessionFactoryImplementor sessionFactoryImplementor) throws QueryException
    {
        if (args.size() < 2) {
            throw new IllegalArgumentException("The function must be passed 2 arguments");
        }
        String jsonColumn = (String) args.get(0);
        String jsonField = (String) args.get(1);

        if (jsonField != null && jsonField.contains(".")) {
            String[] parts = jsonField.split("\\.");
            jsonField = parts[parts.length - 1];
        }
        return String.format("%s->>'%s'", jsonColumn, jsonField);
    }

    @Override
    public Type getReturnType(final Type columnType, final Mapping mapping) throws QueryException {
        return new StringType();
    }

    @Override
    public boolean hasArguments() {
        return true;
    }

    @Override
    public boolean hasParenthesesIfNoArguments() {
        return false;
    }
}
