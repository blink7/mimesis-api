package com.prophunt.persistence.jpa.hibernate;

import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.compile.RenderingContext;
import org.hibernate.query.criteria.internal.expression.LiteralExpression;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * Render literals as plain strings.
 */
public class LiteralEscapedStringExpression extends LiteralExpression<String>
{
    private static final long serialVersionUID = 1L;

    public LiteralEscapedStringExpression(final CriteriaBuilder criteriaBuilder, final String literal) {
        super((CriteriaBuilderImpl) criteriaBuilder, String.class, literal);
    }

    @Override
    public String render(final RenderingContext renderingContext) {
        return "'" + this.getLiteral() + "'";
    }
}
