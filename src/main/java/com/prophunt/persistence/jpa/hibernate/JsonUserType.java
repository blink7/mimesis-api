package com.prophunt.persistence.jpa.hibernate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.*;
import java.util.*;

@Slf4j
public class JsonUserType implements UserType {

    public static final String NAME = "com.prophunt.persistence.jpa.hibernate.JsonUserType";

    protected static final int SQL_TYPE = java.sql.Types.OTHER;

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] names, final SharedSessionContractImplementor session,
                              final Object owner) throws HibernateException, SQLException {

        String jsonString = rs.getString(names[0]);

        if (jsonString == null) {
            return null;
        }

        HashMap map = new HashMap();
        ObjectMapper mapper = new ObjectMapper();
        try {
            map = mapper.readValue(jsonString, new TypeReference<HashMap<String, Object>>() {});
        } catch (Exception e) {
            log.error("nullSafeGet", e);
        }
        return map;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void nullSafeSet(final PreparedStatement st, final Object value, final int index,
                            final SharedSessionContractImplementor session) throws HibernateException, SQLException {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = null;
        if (value != null) {
            map = (Map<String, Object>) value;
        }

        String json = null;
        try {
            json = mapper.writeValueAsString(map);
        } catch (Exception e) {
            log.error("nullSafeSet", e);
        }
        st.setObject(index, json, SQL_TYPE);
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
        return cached;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object deepCopy(final Object o) throws HibernateException {
        Map<String, Object> m = new HashMap<>();
        if (o != null) {
            m.putAll(((Map<String, Object>) o));
        }
        return o == null ? null : m;
    }

    @Override
    public Serializable disassemble(final Object o) throws HibernateException {
        return (Serializable) o;
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        return Objects.equals(x, y);
    }

    @Override
    public int hashCode(final Object o) throws HibernateException {
        return o == null ? 0 : o.hashCode();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
        return original;
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Class<Map<String, Object>> returnedClass() {
        return (Class) HashMap.class;
    }

    @Override
    public int[] sqlTypes() {
        return new int[]{SQL_TYPE};
    }
}
