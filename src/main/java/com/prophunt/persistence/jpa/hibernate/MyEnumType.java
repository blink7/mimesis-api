package com.prophunt.persistence.jpa.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.DynamicParameterizedType;
import org.hibernate.usertype.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

public class MyEnumType implements UserType, DynamicParameterizedType, Serializable {

    private static final long serialVersionUID = -5546857217339313813L;

    private static final Logger LOG = LoggerFactory.getLogger(MyEnumType.class);

    public static final String ENUM_TYPE_NAME = "com.prophunt.persistence.jpa.hibernate.MyEnumType";

    @SuppressWarnings("rawtypes")
    private Class<? extends Enum> enumClass;

    private final int sqlType = Types.OTHER; // before any guessing

    @Override
    public int[] sqlTypes() {
        return new int[]{sqlType};
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Class<? extends Enum> returnedClass() {
        return enumClass;
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        return x == y;
    }

    @Override
    public int hashCode(final Object x) throws HibernateException {
        return x == null ? 0 : x.hashCode();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] strings, SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws HibernateException, SQLException {
        final String name = resultSet.getString(strings[0]);
        try {
            return name == null ? null : Enum.valueOf(enumClass, name);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Unknown name value for enum " + enumClass + ": " + name, e);
        }
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object o, int i, SharedSessionContractImplementor sharedSessionContractImplementor) throws HibernateException, SQLException {
        final Object jdbcValue = o == null ? null : o.toString();
        if (jdbcValue == null) {
            preparedStatement.setNull(i, MyEnumType.this.sqlType);
            return;
        }
        preparedStatement.setObject(i, jdbcValue, MyEnumType.this.sqlType);
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(final Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
        return original;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setParameterValues(final Properties parameters) {
        final ParameterType reader = (ParameterType) parameters.get(PARAMETER_TYPE);
        if (reader != null) {
            enumClass = reader.getReturnedClass().asSubclass(Enum.class);
            LOG.debug("Registering enumType " + enumClass);
        }
    }
}
