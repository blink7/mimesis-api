package com.prophunt.persistence.jpa.hibernate;

import org.hibernate.dialect.PostgreSQL95Dialect;

/**
 * Register the custom functions into PgDialect.
 */
public class PgSQLCustomDialect extends PostgreSQL95Dialect
{
    public static final String ARRAY_CONTAINS = "arrayContains";
    public static final String GET_JSON_VALUE_BY_KEY = "getJsonValueByKey";

    public PgSQLCustomDialect() {
        registerFunction(ARRAY_CONTAINS, new PgArrayContainsFunction());
        registerFunction(GET_JSON_VALUE_BY_KEY, new PgGetJsonValueByKey());
    }
}
