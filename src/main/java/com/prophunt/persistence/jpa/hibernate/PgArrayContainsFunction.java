package com.prophunt.persistence.jpa.hibernate;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

import java.util.List;
import java.util.stream.Collectors;

public class PgArrayContainsFunction implements SQLFunction
{
    @SuppressWarnings("rawtypes")
    @Override
    public String render(final Type type, final List args, final SessionFactoryImplementor sessionFactoryImplementor) throws QueryException
    {
        if (args.size() < 2) {
            throw new IllegalArgumentException("The function must be passed 2 arguments");
        }
        String field = (String) args.get(0);
        @SuppressWarnings("unchecked")
        List<String> values = (List<String>) args.subList(1, args.size()).stream().map(Object::toString).collect(Collectors.toList());

        String value = null;
        if (args.get(1).getClass() == String.class) {
            value = String.join(", ", values.toArray(new String[]{}));
        }

        return field + " @> ARRAY[" + value + "] AND TRUE ";
    }

    @Override
    public Type getReturnType(final Type columnType, final Mapping mapping) throws QueryException {
        return new BooleanType();
    }

    @Override
    public boolean hasArguments() {
        return true;
    }

    @Override
    public boolean hasParenthesesIfNoArguments() {
        return true;
    }
}
