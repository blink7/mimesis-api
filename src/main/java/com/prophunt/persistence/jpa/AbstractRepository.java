package com.prophunt.persistence.jpa;

import com.blazebit.persistence.CriteriaBuilderFactory;
import com.prophunt.persistence.ErrorCode;
import com.prophunt.persistence.LongModel;
import com.prophunt.persistence.PropHuntError;
import com.prophunt.persistence.jpa.hibernate.LiteralEscapedStringExpression;
import com.prophunt.persistence.jpa.hibernate.LiteralStringExpression;
import com.prophunt.persistence.jpa.hibernate.PgSQLCustomDialect;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.validation.ConstraintViolationException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractRepository<T extends LongModel> {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractRepository.class);

    private static final String SAVING = "Saving %s";

    @Autowired
    private EntityManager em;

    @Autowired
    private CriteriaBuilderFactory criteriaBuilderFactory;

    protected EntityManager getEntityManager() {
        return em;
    }

    protected CriteriaBuilder getCriteriaBuilder() {
        return em.getCriteriaBuilder();
    }

    protected CriteriaQuery<T> createQuery() {
        return getCriteriaBuilder().createQuery(getEntityType());
    }

    protected CriteriaBuilderFactory getCriteriaBuilderFactory() {
        return criteriaBuilderFactory;
    }

    /**
     * Repository model class.
     *
     * @return repository model class
     */
    protected abstract Class<T> getEntityType();

    /**
     * Saves or updates a model in the database.
     *
     * @param model to be saved/updated
     * @return persisted model
     */
    public T saveOrUpdate(final T model) {
        LOG.debug("Saving/Update {}", getEntityType().toString());
        return em.merge(model);
    }

    /**
     * Saves or updates a model and forces the data to be persisted in the database immediately.
     *
     * @param model to be saved/updated
     * @return persisted model
     */
    public T saveOrUpdateAndFlush(final T model) {
        LOG.debug("Saving/Update and Flush {}", getEntityType().toString());
        try {
            T entity = em.merge(model);
            em.flush();
            em.refresh(entity);
            return entity;
        } catch (PersistenceException re) {
            if (re.getCause() != null && re.getCause() instanceof ConstraintViolationException) {
                var ce = (ConstraintViolationException) re.getCause();
                if (ce.getCause() != null) {
                    throw new javax.validation.ConstraintViolationException(ce.getCause().getMessage(), null);
                }
            }
            throw re;
        }
    }

    /**
     * Forces synchronization with database.
     */
    public void flush() {
        LOG.debug("Flush {}", getEntityType().toString());
        em.flush();
    }

    /**
     * Saves a model in the database.
     *
     * @param model to be saved
     */
    public void save(final T model) {
        LOG.debug("Saving {}", getEntityType().toString());
        em.persist(model);
    }

    /**
     * Saves a model and forces the data to be persisted in the database immediately.
     *
     * @param model to be saved
     */
    public void saveAndFlush(final T model) {
        LOG.debug("Saving and flush {}", getEntityType().toString());
        em.persist(model);
        em.flush();
    }

    /**
     * Fetch all records of model <T>.
     *
     * @return records of model <T>
     */
    public List<T> findAll() {
        LOG.debug("Finding all {}", getEntityType().toString());
        var cq = em.getCriteriaBuilder().createQuery(getEntityType());
        cq.select(cq.from(getEntityType()));
        setFiltroDefault(cq);
        return em.createQuery(cq).getResultList();
    }

    /**
     * Defines a default filter for queries. For example: active = true.
     *
     * @param cq jpa's criteria query
     */
    protected void setFiltroDefault(final CriteriaQuery<T> cq) {
    }

    /**
     * Find reference for given ID.
     *
     * @param id model's id
     * @return model reference
     */
    public T getReference(final Long id) {
        LOG.debug("Finding {} reference for id {}", getEntityType().toString(), id);
        return em.getReference(getEntityType(), id);
    }

    /**
     * Remove model from database.
     *
     * @param model to be removed
     */
    public void remove(final T model) {
        em.remove(model);
    }

    /**
     * Remove model from database and forces synchronization with database.
     *
     * @param model to be removed
     */
    public void removeAndFlush(final T model) {
        em.remove(model);
        em.flush();
    }

    /**
     * Find model for a given id.
     *
     * @param id   database identifier
     * @param lock indicates if record must be locked
     * @return model for given ID if found, {@code null} if id itself is {@code null}.
     */
    public Optional<T> findById(final Long id, final boolean lock) {
        LOG.debug("Register search [id={}, lock={}]", java.util.Optional.ofNullable(id), lock);
        T value = null;
        if (id != null) {
            if (lock) {
                value = em.find(getEntityType(), id, LockModeType.PESSIMISTIC_WRITE);
            }
            else {
                value = em.find(getEntityType(), id);
            }
//            checkNotNull(id, value);
        }
        return Optional.ofNullable(value);
    }

    protected void checkNotNull(final Long id, final T value) {
        if (value == null) {
            throw new PropHuntError(ErrorCode.RecordNotFoundException, getEntityType().getSimpleName(), id);
        }
    }

    /**
     * Verifies if exists a record in the database for the given query.
     *
     * @param query   query to be tested
     * @param builder CriteriaBuilder
     * @return {@code true} if at least one record found, {@code false} otherwise
     */
    protected boolean exists(final CriteriaQuery<Integer> query, final CriteriaBuilder builder) {
        return getSingleResult(query.select(builder.literal(1))) != null;
    }

    /**
     * Get a single result from a query.
     *
     * @param <E> type for query
     * @param cq  CriteriaQuery
     * @return record if found, {@code null} otherwise
     */
    protected <E> E getSingleResult(final CriteriaQuery<E> cq) {
        var list = getEntityManager().createQuery(cq).setMaxResults(1).getResultList();
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    protected <E> E getSingleResultAndLock(final CriteriaQuery<E> cq) {
        var list = getEntityManager().createQuery(cq).setMaxResults(1).setLockMode(LockModeType.PESSIMISTIC_WRITE).getResultList();
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    protected <E> E getSingleResultWithoutMax(final CriteriaQuery<E> cq) {
        var list = getEntityManager().createQuery(cq).getResultList();
        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    /**
     * Saves or updates a model.
     *
     * @param model to be saved/updated
     * @param flush forces the data to be persisted in the database immediately
     * @return persisted model
     */
    public T saveOrUpdate(final T model, final boolean flush) {
        LOG.debug(SAVING, model.getClass().toString());
        T entity = em.merge(model);
        if (flush) {
            em.flush();
        }
        return entity;
    }

    /**
     * Saves or updates models.
     *
     * @param models to be saved/updated
     */
    public void saveOrUpdateAll(final Iterable<T> models) {
        saveOrUpdateAll(models, false);
    }

    /**
     * Saves or updates models.
     *
     * @param models to be saved/updated
     * @param flush  forces the data to be persisted in the database immediately
     */
    public void saveOrUpdateAll(final Iterable<T> models, final boolean flush) {
        models.forEach(model -> saveOrUpdate(model, flush));
    }

    /**
     * Saves a model.
     *
     * @param model to be saved/updated
     * @param flush forces the data to be persisted in the database immediately
     */
    private void save(final T model, final boolean flush) {
        LOG.debug(SAVING, model.getClass().toString());
        em.persist(model);
        if (flush) {
            em.flush();
        }
    }

    /**
     * Saves models.
     *
     * @param models to be saved/updated
     */
    public void saveAll(final Iterable<T> models) {
        saveAll(models, false);
    }

    /**
     * Saves models.
     *
     * @param models to be saved/updated
     * @param flush  forces the data to be persisted in the database immediately
     */
    public void saveAll(final Iterable<T> models, final boolean flush) {
        models.forEach(model -> save(model, flush));
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public T fetchFields(final Long modelId, final Attribute... attributesToFetch) {
        final var builder = getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<T> query = builder.createQuery(getEntityType());

        Root<T> root = query.from(getEntityType());
        for (Attribute a : attributesToFetch) {
            if (SingularAttribute.class.isAssignableFrom(a.getClass())) {
                root.fetch((SingularAttribute) a, JoinType.LEFT);
            }
            else if (PluralAttribute.class.isAssignableFrom(a.getClass())) {
                root.fetch((PluralAttribute) a, JoinType.LEFT);
            }
        }
        query.where(builder.equal(root.get(getIdAttribute()), modelId));

        return getSingleResult(query);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public T fetchFields(final Long modelId, final Map<Attribute, Attribute[]> attributesToFetch) {
        final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<T> query = builder.createQuery(getEntityType());

        Root<T> root = query.from(getEntityType());

        for (Attribute joinAtt : attributesToFetch.keySet()) {
            Join<T, ?> j = null;
            if (SingularAttribute.class.isAssignableFrom(joinAtt.getClass())) {
                j = (Join) root.fetch((SingularAttribute) joinAtt, JoinType.LEFT);
            }
            else if (CollectionAttribute.class.isAssignableFrom(joinAtt.getClass())) {
                j = (Join) root.fetch((CollectionAttribute) joinAtt, JoinType.LEFT);
            }
            if (attributesToFetch.get(joinAtt) != null) {
                for (Attribute a : attributesToFetch.get(joinAtt)) {
                    if (SingularAttribute.class.isAssignableFrom(a.getClass())) {
                        j.fetch((SingularAttribute) a, JoinType.LEFT);
                    }
                    else if (PluralAttribute.class.isAssignableFrom(a.getClass())) {
                        j.fetch((PluralAttribute) a, JoinType.LEFT);
                    }
                }
            }
        }

        query.where(builder.equal(root.get(getIdAttribute()), modelId));

        return getSingleResult(query);
    }

    public SingularAttribute<T, Long> getIdAttribute() {
        throw new NotImplementedException("getIdAttribute() is not implemented");
    }

    public Timestamp getCurrentDBTime() {
        return ((TimestampItem) em.createNativeQuery("SELECT CURRENT_TIMESTAMP AS now", TimestampItem.class).getSingleResult()).getTimestamp();
    }

    public Timestamp getDBTodayStartOfDay() {
        return ((TimestampItem) em.createNativeQuery("SELECT CAST(CURRENT_DATE as timestamp) AS now", TimestampItem.class).getSingleResult()).getTimestamp();
    }

    /**
     * Build the expression in order to search a JSON key inside a JSONB column.
     *
     * @param builder  criteria builder
     * @param jsonPath path of the JSONB column
     * @param jsonKey  key wanted in the search
     * @return expression that returns the value for a given key
     */
    public Expression<String> buildExpressionJsonValueByKey(final CriteriaBuilder builder, final Path<Object> jsonPath, final String jsonKey) {
        return builder.function(
                PgSQLCustomDialect.GET_JSON_VALUE_BY_KEY,
                String.class,
                jsonPath,
                new LiteralStringExpression(builder, jsonKey));
    }

    public Expression<Boolean> buildExpressionArrayContains(final CriteriaBuilder builder, final Path<?> path, final String value) {
        return builder.function(
                PgSQLCustomDialect.ARRAY_CONTAINS,
                Boolean.class,
                path,
                new LiteralEscapedStringExpression(builder, value));
    }
}
