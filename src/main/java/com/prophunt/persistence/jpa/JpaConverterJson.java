package com.prophunt.persistence.jpa;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Converter
public class JpaConverterJson implements AttributeConverter<Object, String> {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @SuppressWarnings("unchecked")
    @Override
    public String convertToDatabaseColumn(Object attribute) {
        Map<String, Object> map = null;
        if (attribute != null) {
            map = (Map<String, Object>) attribute;
        }

        String json = null;
        try {
            json = objectMapper.writeValueAsString(map);
        } catch (Exception e) {
            log.error("nullSafeSet", e);
        }
        return json;
    }

    @Override
    public Object convertToEntityAttribute(String dbData) {
        HashMap map = new HashMap();
        try {
            map = objectMapper.readValue(dbData, new TypeReference<HashMap<String, Object>>() {});
        } catch (Exception e) {
            log.error("nullSafeGet", e);
        }
        return map;
    }
}
