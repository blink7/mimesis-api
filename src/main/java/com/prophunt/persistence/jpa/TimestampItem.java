package com.prophunt.persistence.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

//@Entity
public class TimestampItem
{
//    @Id
//    @Column(name = "now", insertable = false, updatable = false)
    private Timestamp timestamp;

    public TimestampItem()
    {
    }

    public Timestamp getTimestamp()
    {
        return timestamp;
    }
}
