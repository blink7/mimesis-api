package com.prophunt.persistence;

import com.blazebit.persistence.ObjectBuilder;
import com.blazebit.persistence.SelectBuilder;
import com.prophunt.persistence.dto.PlayerItem;
import com.prophunt.persistence.jpa.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public class PlayerRepository extends AbstractRepository<Player> {

    @Override
    protected Class<Player> getEntityType() {
        return Player.class;
    }

    public boolean checkPlayerExist(@NotNull Long playerId) {
        final var builder = getEntityManager().getCriteriaBuilder();
        final var query = builder.createQuery(Integer.class);
        final var player = query.from(getEntityType());

        return exists(query
                .where(builder.equal(player.get(Player_.PLAYER_ID), playerId)), builder);
    }

    public boolean checkPlayerExist(@NotNull String uniqueNetId) {
        final var builder = getEntityManager().getCriteriaBuilder();
        final var query = builder.createQuery(Integer.class);
        final var player = query.from(getEntityType());

        return exists(query
                .where(builder.equal(player.get(Player_.UNIQUE_NET_ID), uniqueNetId)), builder);
    }

    public Optional<Player> findByUniqueNetId(@NotNull String uniqueNetId) {
        final var builder = getEntityManager().getCriteriaBuilder();
        final var query = builder.createQuery(getEntityType());
        final var player = query.from(getEntityType());

        return Optional.ofNullable(getSingleResult(query.select(player)
                .where(builder.equal(player.get(Player_.UNIQUE_NET_ID), uniqueNetId))));
    }

    public Optional<Long> findPlayerIdByUniqueNetId(@NotNull String uniqueNetId) {
        final var builder = getEntityManager().getCriteriaBuilder();
        final var query = builder.createQuery(Long.class);
        final var player = query.from(getEntityType());

        return Optional.ofNullable(getSingleResult(query.select(player.get(Player_.PLAYER_ID))
                .where(builder.equal(player.get(Player_.UNIQUE_NET_ID), uniqueNetId))));
    }

    public List<PlayerItem> getPlayerItems() {
        return getCriteriaBuilderFactory().create(getEntityManager(), Tuple.class)
                .from(getEntityType())
                .selectNew(new ObjectBuilder<PlayerItem>() {
                    @Override
                    public <X extends SelectBuilder<X>> void applySelects(X queryBuilder) {
                        queryBuilder
                                .select(Player_.PLAYER_ID)
                                .select(Player_.PLAYER_NAME)
                                .select(Player_.CHARACTER_LEVEL)
                                .select(Player_.ONLINE_SUBSYSTEM);
                    }

                    @Override
                    public PlayerItem build(Object[] tuple) {
                        return new PlayerItem(
                                (Long) tuple[0],
                                (String) tuple[1],
                                (Integer) tuple[2] ,
                                (OnlineSubsystem) tuple[3]);
                    }

                    @Override
                    public List<PlayerItem> buildList(List<PlayerItem> list) {
                        return list;
                    }
                })
                .orderByAsc(Player_.PLAYER_ID)
                .getResultList();
    }

    public boolean removeById(@NotNull Long playerId) {
        final var builder = getEntityManager().getCriteriaBuilder();
        final var query = builder.createCriteriaDelete(getEntityType());
        final var player = query.from(getEntityType());
        query.where(builder.equal(player.get(Player_.PLAYER_ID), playerId));

        final var result = getEntityManager().createQuery(query).executeUpdate();
        return result > 0;
    }
}
