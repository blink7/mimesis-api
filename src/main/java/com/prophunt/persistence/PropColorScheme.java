package com.prophunt.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.prophunt.persistence.jpa.JpaConverterJson;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Accessors(chain = true)
@ToString
@Entity
public class PropColorScheme extends LongModel implements Serializable
{
    @ToString.Exclude
    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long propColorSchemeId;

    @Getter
    @Setter
    @Column(nullable = false)
    private Boolean useCustomColor = false;

    @Convert(converter = JpaConverterJson.class)
    private Object bodyColor;

    @Convert(converter = JpaConverterJson.class)
    private Object tailColor;

    @JsonIgnore
    @Override
    public Long getId()
    {
        return propColorSchemeId;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> getBodyColor() {
        if (bodyColor == null) {
            bodyColor = new HashMap<>();
        }
        return (Map<String, Object>) bodyColor;
    }

    public PropColorScheme setBodyColor(final Map<String, Object> bodyColor) {
        this.bodyColor = bodyColor;
        return this;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> getTailColor() {
        if (tailColor == null) {
            tailColor = new HashMap<>();
        }
        return (Map<String, Object>) tailColor;
    }

    public PropColorScheme setTailColor(final Map<String, Object> tailColor) {
        this.tailColor = tailColor;
        return this;
    }

    public PropColorScheme updateStats(final PropColorScheme scheme) {
        useCustomColor = scheme.useCustomColor;
        bodyColor = scheme.bodyColor;
        tailColor = scheme.tailColor;
        return this;
    }
}
