package com.prophunt.persistence;

public enum ErrorCode
{
    BadRequest,
    NotFound,

    // Player APIs
    PlayerNotFoundError,
    PlayerAlreadyExistsError,
    PlayerStatsNotFoundError,

    RecordNotFoundException,
    InvalidArgumentError,
    InternalServerError,
    UnknownError,

    // Storage APIs
    FileStoreError,
    CreateDirectoryError
}
