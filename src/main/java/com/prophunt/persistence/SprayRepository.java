package com.prophunt.persistence;

import com.prophunt.persistence.jpa.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class SprayRepository extends AbstractRepository<Spray> {

    @Override
    protected Class<Spray> getEntityType() {
        return Spray.class;
    }

    public boolean isSprayExist(@NotNull final String uniqueNetId, @NotNull final String md5) {
        final var b = getEntityManager().getCriteriaBuilder();
        final var q = b.createQuery(Integer.class);
        final var spray = q.from(getEntityType());
        final var join = spray.join(Spray_.PLAYER);

        return exists(q
                        .where(
                                b.equal(join.get(Player_.UNIQUE_NET_ID), uniqueNetId),
                                b.equal(spray.get(Spray_.MD5), md5)
                        ),
                b);
    }

    public Optional<Spray> findByPlayerId(@NotNull final String uniqueNetId) {
        final var builder = getEntityManager().getCriteriaBuilder();
        final var query = builder.createQuery(getEntityType());
        final var spray = query.from(getEntityType());
        final var join = spray.join(Spray_.PLAYER);

        return Optional.ofNullable(getSingleResult(query
                .select(spray)
                .where(builder
                        .equal(join.get(Player_.UNIQUE_NET_ID), uniqueNetId)
                )
        ));
    }

    public List<String> removeOldSprays() {
        final var b = getEntityManager().getCriteriaBuilder();
        final var tupleQuery = b.createTupleQuery();
        final var spray = tupleQuery.from(getEntityType());

        List<String> sprayNames = new ArrayList<>();
        List<Long> sprayIds = new ArrayList<>();

        getEntityManager().createQuery(tupleQuery
                .multiselect(
                        spray.get(Spray_.SPRAY_ID),
                        spray.get(Spray_.FILE_NAME)
                ).where(
                        b.lessThan(spray.get(Spray_.LAST_USE_DATE), Timestamp.valueOf(LocalDateTime.now().minusDays(30)))
                )
        ).getResultStream()
                .forEach(tuple -> {
                    sprayIds.add(tuple.get(0, Long.class));
                    sprayNames.add(tuple.get(1, String.class));
                });

        if (!sprayIds.isEmpty()) {
            final var sprayQuery = b.createCriteriaDelete(getEntityType());
            final var updateSprayRoot = sprayQuery.from(getEntityType());
            getEntityManager().createQuery(sprayQuery
                    .where(updateSprayRoot.get(Spray_.SPRAY_ID).in(sprayIds))
            ).executeUpdate();
        }

        return sprayNames;
    }
}
