package com.prophunt.rest;

import com.prophunt.business.SprayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping(path = "/spray",
        consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE},
        produces = {MediaType.IMAGE_PNG_VALUE, MediaType.TEXT_PLAIN_VALUE})
public class SprayController {

    private final SprayService service;

    @Autowired
    public SprayController(SprayService service) {
        this.service = service;
    }

    @PostMapping()
    public ResponseEntity<?> checkSprayExists(@RequestParam("id") final String userId, @RequestBody final String md5) {
        if (service.isSprayExist(userId, md5)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping()
    public String uploadSpray(@RequestParam("id") final String userId, @RequestParam final MultipartFile file) {
        return service.storeSpray(userId, file);
    }

    @GetMapping()
    public Resource downloadSpray(@RequestParam("id") final String userId) {
        return service.loadSpray(userId);
    }
}
