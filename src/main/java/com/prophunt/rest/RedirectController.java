package com.prophunt.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class RedirectController {

    @GetMapping("/PropHuntCDN/ContentBuild.json")
    public RedirectView getContentBuild() {
        return new RedirectView("http://localhost:80/PropHuntCDN/ContentBuild.json");
    }
}
