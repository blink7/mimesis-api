package com.prophunt.rest;

import com.prophunt.business.PlayerService;
import com.prophunt.persistence.*;
import com.prophunt.persistence.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/player",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class PlayerController {

    private final PlayerService service;

    @Autowired
    public PlayerController(PlayerService service) {
        this.service = service;
    }

    @PostMapping("/create")
    public PlayerResponse createPlayer(@RequestBody final PlayerRequest playerRequest) {
        return service.createPlayer(playerRequest);
    }

    @GetMapping()
    public PlayerResponse getPlayer(@RequestParam("id") final String userId) {
        return PlayerResponse.fromModel(service.getPlayer(userId));
    }

    @GetMapping("/exp")
    public PlayerExp getExp(@RequestParam("id") final String userId) {
        return service.getExp(userId);
    }

    @PutMapping("/exp")
    public void updateExp(@RequestParam("id") final String userId, @RequestBody final PlayerExp playerExp) {
        service.updateExp(userId, playerExp);
    }

    @PutMapping("/abilities")
    public void updateAbilities(@RequestBody final Player player) {
        service.updateAbilities(player);
    }

    @GetMapping("/all")
    public List<PlayerItem> getAllPlayers() {
        return service.getPlayerItems();
    }

    @DeleteMapping()
    public void removePlayer(@RequestParam("id") final Long playerId) {
        service.removePlayer(playerId);
    }
}
