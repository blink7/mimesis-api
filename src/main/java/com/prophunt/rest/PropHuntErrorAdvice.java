package com.prophunt.rest;

import com.prophunt.persistence.ErrorCode;
import com.prophunt.persistence.PropHuntError;
import com.prophunt.resource.i18n.ErrorBundle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class PropHuntErrorAdvice {

    @Autowired
    private ErrorBundle bundle;

    @ResponseBody
    @ExceptionHandler(PropHuntError.class)
    protected ResponseEntity<Object> handlePropHuntError(PropHuntError exception) {
        final var error = new HashMap<String, Object>();
        error.put("error", exception.getErrorCode().toString());

        if (exception.getOwnMessage() != null) {
            error.put("message", exception.getOwnMessage());
            log.error(exception.getOwnMessage(), exception);
        }
        else {
            String message = bundle.getErrorMessageFromCode(exception.getErrorCode().toString());
            if (exception.getArgs() != null) {
                message = String.format(message, exception.getArgs());
            }
            log.error(message, exception);
            error.put("message", message);
        }
        error.put("args", exception.getArgs());

        HttpStatus status;
        if (exception.getErrorCode().toString().toLowerCase().contains("notfound")) {
            status = HttpStatus.NOT_FOUND;
        }
        else {
            status = HttpStatus.BAD_REQUEST;
        }
        return ResponseEntity.status(status).contentType(MediaType.APPLICATION_JSON).body(error);
    }

    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handlePropHuntError(ConstraintViolationException exception) {
        final var constraintViolations = exception.getConstraintViolations();

        List<String> violations = null;
        if (constraintViolations != null) {

            violations = constraintViolations.stream()
                    .map(PropHuntErrorAdvice::transformConstraintViolationMessage)
                    .peek(log::error)
                    .collect(Collectors.toList());
        }

        final Map<String, Object> error = new HashMap<>();

        error.put("error", ErrorCode.InvalidArgumentError.toString());
        error.put("message", violations == null ? exception.getMessage() : String.join("<br />", violations));
        // error.put("message", "You need to fill the mandatory fields");

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    private static String transformConstraintViolationMessage(final ConstraintViolation<?> e) {
        final var leafObject = e.getLeafBean();
        String leafObjectName = null;
        if (leafObject != null) {
            leafObjectName = leafObject.getClass().getSimpleName();
        }
        String pathString = e.getPropertyPath().toString();
        if (pathString.contains(".")) {
            pathString = pathString.substring(pathString.lastIndexOf(".") + 1);
        }

        return String.format("[%s:%s:%s] InvalidValue[%s] Message[%s]",
                e.getRootBeanClass().getSimpleName(),
                leafObjectName,
                pathString,
                e.getInvalidValue(),
                e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(PersistenceException.class)
    protected ResponseEntity<Object> handlePropHuntError(PersistenceException exception) {
        final var error = new HashMap<String, Object>();
        log.error(exception.getMessage(), exception);
        error.put("error", ErrorCode.InternalServerError.toString());

        Throwable ex = exception;
        while (ex.getCause() != null) {
            ex = ex.getCause();
        }
        error.put("message", ex.getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON).body(error);
    }

    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<Object> handlePropHuntError(RuntimeException exception) {
        log.error("Internal server error", exception);

        final var error = new HashMap<String, Object>();
        error.put("error", ErrorCode.UnknownError.toString());
        error.put("message", exception != null && exception.getMessage() != null
                ? exception.getMessage() : "Internal server error");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON).body(error);
    }
}
