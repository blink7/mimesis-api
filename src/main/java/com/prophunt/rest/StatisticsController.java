package com.prophunt.rest;

import com.prophunt.business.StatisticsService;
import com.prophunt.persistence.PlayerStats;
import com.prophunt.persistence.RatingType;
import com.prophunt.persistence.dto.RatingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/stats",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class StatisticsController {

    private final StatisticsService service;

    @Autowired
    public StatisticsController(StatisticsService service) {
        this.service = service;
    }

    @GetMapping("/today-rating")
    public List<RatingResponse> getTodayRating(@RequestParam final RatingType type,
                                        @RequestParam(required = false, defaultValue = "10") final Integer limit,
                                        @RequestParam("id") final String userId) {
        return service.getTodayRating(type, limit, userId);
    }

    @GetMapping()
    public PlayerStats getStats(@RequestParam("id") final String userId) {
        return service.getStats(userId);
    }

    @PutMapping()
    public void updateStats(@RequestParam("id") final String userId,  @RequestBody final PlayerStats stats) {
        service.updateStats(userId, stats);
    }
}
