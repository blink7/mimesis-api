package com.prophunt.rest;

import com.prophunt.business.CustomizationService;
import com.prophunt.persistence.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/customization",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomizationController {

    private final CustomizationService service;

    @Autowired
    public CustomizationController(CustomizationService service) {
        this.service = service;
    }

    @GetMapping("/prop")
    public PropColorScheme getPropColorScheme(@RequestParam("id") final String userId) {
        return service.getPropColorScheme(userId);
    }

    @PutMapping("/prop")
    public void updatePropColorScheme(@RequestParam("id") final String userId,  @RequestBody final PropColorScheme scheme) {
        service.updatePropColorScheme(userId, scheme);
    }
}
