package com.prophunt.resource.i18n;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.annotation.PostConstruct;
import java.util.ResourceBundle;

@Component
@RequestScope
public class ErrorBundle extends ResourceBundles {

    private static final long serialVersionUID = 5281100667959417320L;

    private ResourceBundle bundle;

    @PostConstruct
    public void init() {
        bundle = ResourceBundle.getBundle("error/ErrorBundle", getLocale());
    }

    public String getErrorMessageFromCode(final String errorCode) {
        return bundle.containsKey(errorCode) ? bundle.getString(errorCode) : errorCode;
    }
}
