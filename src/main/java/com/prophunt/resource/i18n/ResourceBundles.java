package com.prophunt.resource.i18n;

import com.prophunt.resource.locale.RequestLocale;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ResourceBundle.Control;
import java.util.*;

/**
 * Abstractions for PropHunt bundles.
 */
public abstract class ResourceBundles implements Serializable {

    private static final long serialVersionUID = 8959217051333049498L;

    @Autowired
    private RequestLocale requestLocale;

    Locale getLocale() {
        if (requestLocale != null && requestLocale.getLocale() != null) {
            return requestLocale.getLocale();
        }
        else {
            return Locale.US;
        }
    }

    public static class UTF8Control extends Control {
        @Override
        public ResourceBundle newBundle(final String baseName, final Locale locale, final String format,
                                        final ClassLoader loader, final boolean reload)
                throws IllegalAccessException, InstantiationException, IOException {
            // The below is a copy of the default implementation.
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");
            ResourceBundle bundle = null;
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            }
            else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                try {
                    // Only this line is changed to make it read properties files as UTF-8.
                    bundle = new PropertyResourceBundle(new InputStreamReader(stream, StandardCharsets.UTF_8));
                } finally {
                    stream.close();
                }
            }
            return bundle;
        }
    }
}
