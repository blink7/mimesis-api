package com.prophunt.resource.locale;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.io.Serializable;
import java.util.Locale;

@Component
@RequestScope
public class RequestLocale implements Serializable {

    private static final long serialVersionUID = -6247441860062373239L;

    private Locale locale;

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale;
    }

    public void setLocale(final String language, final String country) {
        this.locale = new Locale(language, country);
    }
}
